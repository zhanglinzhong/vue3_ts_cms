import { IForm } from '@/base-ui/form'

export const modalConfig: IForm = {
  title: '新建部门',
  formItems: [
    {
      field: 'name',
      type: 'input',
      label: '部门名称',
      placeholder: '请输入部门名称'
    },
    {
      field: 'leader',
      type: 'input',
      label: '部门领导',
      placeholder: '请输入部门领导'
    },
    {
      field: 'parent_id',
      type: 'select',
      label: '上级部门',
      placeholder: '请选择上级部门',
      options: []
    }
  ],
  colLayout: {
    span: 24
  },
  itemStyle: {
    padding: '0px 10px'
  }
}
