export const contentTableConfig = {
  title: '用户列表',
  propList: [
    { prop: 'name', label: '用户名', minWidth: '100' },
    { prop: 'realname', label: '真是姓名', minWidth: '100' },
    { prop: 'cellphone', label: '电话号码', minWidth: '120' },
    { prop: 'enable', label: '状态', minWidth: '100', slotName: 'status' },
    // { prop: 'department', label: '用户名', minWidth: '100' },
    // { prop: 'role', label: '用户名', minWidth: '100' },
    {
      prop: 'created_at',
      label: '创建时间',
      minWidth: '250',
      slotName: 'createAt'
    },
    {
      prop: 'update_at',
      label: '更新时间',
      minWidth: '250',
      slotName: 'updateAt'
    },
    { label: '操作', minWidth: '120', slotName: 'handler' }
  ],

  showIndexColumn: true,
  showSelectColumn: true
}
