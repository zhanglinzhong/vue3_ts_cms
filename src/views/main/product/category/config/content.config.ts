export const contentTableConfig = {
  title: '商品类别',
  propList: [
    { prop: 'name', label: '商品类别', minWidth: '100' },
    // { prop: 'department', label: '用户名', minWidth: '100' },
    // { prop: 'role', label: '用户名', minWidth: '100' },
    {
      prop: 'created_at',
      label: '创建时间',
      minWidth: '250',
      slotName: 'createAt'
    },
    {
      prop: 'update_at',
      label: '更新时间',
      minWidth: '250',
      slotName: 'updateAt'
    },
    { label: '操作', minWidth: '120', slotName: 'handler' }
  ],

  showIndexColumn: true,
  showSelectColumn: true
}
