import { IForm } from '@/base-ui/form'

export const modalConfig: IForm = {
  title: '新建商品类别',
  formItems: [
    {
      field: 'name',
      type: 'input',
      label: '商品类别',
      placeholder: '请输入商品类别'
    }
  ],
  colLayout: {
    span: 24
  },
  itemStyle: {
    padding: '0px 10px'
  }
}
