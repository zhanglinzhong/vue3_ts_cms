// 接口请求拦截器
import type { AxiosRequestConfig, AxiosResponse } from 'axios'

export interface LZRequestInterceptors<T = AxiosResponse> {
  requestInterceptor?: (config: AxiosRequestConfig) => AxiosRequestConfig
  requestInterceptorCatch?: (error: any) => any
  responseInterceptor?: (config: T) => T
  responseInterceptorCatch?: (error: any) => any
}

export interface LZRequestConfig<T = AxiosResponse> extends AxiosRequestConfig {
  interceptors?: LZRequestInterceptors<T>
  showLoading?: boolean
}
