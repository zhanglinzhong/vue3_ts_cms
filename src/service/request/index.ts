// 封装axiox
import axios from 'axios'
import type { AxiosInstance } from 'axios'
import { useRouter } from 'vue-router'
import localCache from '@/utils/cache'

//将类型单独放到types.ts文件夹中
import { LZRequestInterceptors, LZRequestConfig } from './types'

import { ElLoading } from 'element-plus'
import { ILoadingInstance } from 'element-plus/lib/el-loading/src/loading.type'
// // 接口请求拦截器
// interface LZRequestInterceptors {
//   requestInterceptor?: (config: AxiosRequestConfig) => AxiosRequestConfig
//   requestInterceptorCatch?: (error: any) => any
//   responseInterceptor?: (config: AxiosResponse) => AxiosResponse
//   responseInterceptorCatch?: (error: any) => any
// }

// interface LZRequestConfig extends AxiosRequestConfig {
//   interceptors?: LZRequestInterceptors
// }

class LZRequest {
  instance: AxiosInstance
  interceptors?: LZRequestInterceptors
  showLoading: boolean
  loading?: ILoadingInstance

  constructor(config: LZRequestConfig) {
    this.instance = axios.create(config)
    this.interceptors = config.interceptors
    this.showLoading = config.showLoading ?? true

    // 从config中取出的拦截器对应的实例拦截器
    this.instance.interceptors.request.use(
      this.interceptors?.requestInterceptor,
      this.interceptors?.requestInterceptorCatch
    )
    this.instance.interceptors.response.use(
      this.interceptors?.responseInterceptor,
      this.interceptors?.responseInterceptorCatch
    )

    // 添加所有实例的拦截器
    this.instance.interceptors.request.use(
      (config) => {
        // console.log('所有实例都有的拦截器：请求拦截成功')
        if (this.showLoading) {
          this.loading = ElLoading.service({
            lock: true,
            text: '正在拼命加载中...',
            background: 'rgba(0,0,0,0.5)'
          })
        }

        return config
      },
      (err) => {
        // console.log('所有实例都有的拦截器：请求拦截失败')
        return err
      }
    )
    this.instance.interceptors.response.use(
      (res) => {
        // console.log('所有实例都有的拦截器：响应拦截成功')
        // 将loading移除
        // setTimeout()
        this.loading?.close()

        const data = res.data
        // console.log('后端数据返回拦截', data)
        // console.log('后端数据返回拦截', res)
        if (res.status === 401) {
          // 跳到首页 清空缓存
          const router = useRouter()
          localCache.deleteCache('token') //删除token之后，需要跳转到登陆页
          router.push('/main')
          return
        }
        if (data.code !== 0) {
          // console.log('请求失败，错误信息')
          return data
        } else {
          return data
        }
      },
      (err) => {
        // console.log('所有实例都有的拦截器：响应拦截失败')
        // 将loading移除
        this.loading?.close()
        return err
      }
    )
  }

  request<T = any>(config: LZRequestConfig<T>): Promise<T> {
    return new Promise((resolve, reject) => {
      // 单个请求也可以传入拦截器,单个请求对请求config的处理
      if (config.interceptors?.requestInterceptor) {
        config = config.interceptors.requestInterceptor(config)
      }

      if (config.showLoading === false) {
        this.showLoading = config.showLoading
      }

      this.instance
        .request<any, T>(config)
        .then((res) => {
          // 单个请求对数据的处理
          if (config.interceptors?.responseInterceptor) {
            res = config.interceptors.responseInterceptor(res)
          }
          // console.log(res)
          // 每次请求完成之后，需要把把showloading在改为true,否则下次请求依然是false
          this.showLoading = true

          // 将结果通过resolve返回出去
          resolve(res)
        })
        .catch((err) => {
          this.showLoading = true
          reject(err)
          return err
        })
    })
  }

  get<T = any>(config: LZRequestConfig<T>): Promise<T> {
    return this.request<T>({ ...config, method: 'GET' })
  }

  post<T = any>(config: LZRequestConfig<T>): Promise<T> {
    return this.request<T>({ ...config, method: 'POST' })
  }

  delete<T = any>(config: LZRequestConfig<T>): Promise<T> {
    return this.request<T>({ ...config, method: 'DELETE' })
  }

  patch<T = any>(config: LZRequestConfig<T>): Promise<T> {
    return this.request<T>({ ...config, method: 'PATCH' })
  }
}

export default LZRequest
