import lzRequest from '../../index'

enum DashboardAPI {
  amountList = '/goods/amount/list',
  categoryGoodsCount = '/goods/category/count',
  categoryGoodsSales = '/goods/category/sale',
  categoryGoodsFavor = '/goods/category/favor',
  addressGoodsSales = '/goods/address/sale'
}
export function getAmountList() {
  return lzRequest.get({
    url: DashboardAPI.amountList
  })
}

export function getCategoryGoodsCount() {
  return lzRequest.get({
    url: DashboardAPI.categoryGoodsCount
  })
}

export function getCategoryGoodsSale() {
  return lzRequest.get({
    url: DashboardAPI.categoryGoodsSales
  })
}

export function getCategoryGoodsFavor() {
  return lzRequest.get({
    url: DashboardAPI.categoryGoodsFavor
  })
}

export function getAddressGoodsSale() {
  return lzRequest.get({
    url: DashboardAPI.addressGoodsSales
  })
}
