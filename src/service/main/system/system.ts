import lzRequest from '@/service'

import { IResDate } from '../../types'

export function getPageListData(url: string, queryInfo: any) {
  return lzRequest.post<IResDate>({
    url: url,
    data: queryInfo
  })
}

// 根据传来的路由和ID删除对应数据 url: /users/:id
export function deletePageData(url: string) {
  return lzRequest.delete<IResDate>({
    url: url
  })
}

// 新建数据 路由：/pageName
export function createPageData(url: string, newData: any) {
  return lzRequest.post<IResDate>({
    url: url,
    data: newData
  })
}

// 编辑数据 路由：/pageName
export function updatePageData(url: string, editData: any) {
  return lzRequest.patch<IResDate>({
    url: url,
    data: editData
  })
}
