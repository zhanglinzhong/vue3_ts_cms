import lzRequest from '../index'
import { IAccount } from './types'
import { IResDate } from '../types'

enum LoginAPI {
  AccountLogin = '/login',
  LoginUserInfo = '/users/', // /users/1
  UserMenus = '/role/' // /role/1/menu
}

export function accountLoginRequest(account: IAccount) {
  return lzRequest.post<IResDate>({
    url: LoginAPI.AccountLogin,
    data: account
  })
}

export function requestUserInfoById(id: number) {
  return lzRequest.get<IResDate>({
    url: LoginAPI.LoginUserInfo + id,
    showLoading: false
  })
}

export function requestUserMenuByRoleId(id: number) {
  return lzRequest.get<IResDate>({
    url: LoginAPI.UserMenus + id + '/menu',
    showLoading: false
  })
}
