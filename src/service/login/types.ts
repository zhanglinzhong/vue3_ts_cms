export interface IAccount {
  name: string
  password: string
  captId: string
  code: string
}
