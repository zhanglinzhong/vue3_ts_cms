import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'

dayjs.extend(utc)

const date_time_format = 'YYYY-MM-DD HH:mm:ss'

export function formatUtcString(
  utcString: string,
  format: string = date_time_format
) {
  return dayjs.utc(utcString).local().format(format)
}

export function formatTimestamp(
  timestamp: number,
  format: string = date_time_format
) {
  return dayjs(timestamp).format(format)
}
