import { ref } from 'vue'
import PageModal from '@/components/page-modal'

type CallbackFn = (item?: any) => void

export function usePageModal(newCb?: CallbackFn, editCb?: CallbackFn) {
  const pageModalRef = ref<InstanceType<typeof PageModal>>()
  const defaultInfo = ref({})
  const handleNewData = () => {
    defaultInfo.value = {}
    if (pageModalRef.value) {
      pageModalRef.value.dialogVisible = true
    }
    newCb && newCb() //参数有值的时候，进行参数函数调用
  }

  const handleEditData = (item: any) => {
    defaultInfo.value = { ...item }
    // console.log(defaultInfo.value)
    if (pageModalRef.value) {
      pageModalRef.value.dialogVisible = true
    }
    // console.log('用户点击了编辑', item)
    editCb && editCb(item) //参数有值的时候，进行参数函数调用
  }
  return [pageModalRef, defaultInfo, handleNewData, handleEditData]
}
