import { createApp } from 'vue'
import App from './App.vue'

import router from './router'
import store from './store'

//注册全局element-plus
import { globalRegister } from './global'
import 'normalize.css' // 引入全局样式重置
import './assets/css/index.less'

import { setupStore } from './store'

const app = createApp(App)
app.use(globalRegister)

app.use(store)
setupStore()

app.use(router)

app.mount('#app')

// // 网络请求封装后的使用
// import lzRequest from './service'

// lzRequest.request({
//   url: '/home/multidata',
//   method: 'GET',
//   interceptors: {
//     requestInterceptor: (config) => {
//       console.log('单个请求也可以定义拦截器')
//       return config
//     },
//     responseInterceptor: (res) => {
//       console.log('单个响应成功的拦截')
//       return res
//     }
//   },
//   showLoading: false
// })
