import { createStore, Store, useStore as useVuexStore } from 'vuex'

import login from './login/login'
import system from './main/system/system'
import dashboard from './main/analysis/dashboard'

import { getPageListData } from '@/service/main/system/system'

import { IRootState, IStoreType } from './types'

const store = createStore<IRootState>({
  state: () => {
    return {
      name: 'lzz',
      age: 18,
      entireDepartment: [],
      entireRole: [],
      entireMenu: []
    }
  },
  mutations: {
    // mutation 更改state中的数据
    changeEntireDepartment(state, list) {
      state.entireDepartment = list
    },
    changeEntireRole(state, list) {
      state.entireRole = list
    },
    changeEntireMenu(state, list) {
      state.entireMenu = list
    }
  },
  getters: {},
  actions: {
    // 网络请求在actions中发送
    async getInitialDataAction({ commit }) {
      // 1、请求部门和角色数据
      const departmentRes = await getPageListData('/department/list', {
        offset: 0,
        size: 100
      })
      const { list: departmentList } = departmentRes.data
      const roleRes = await getPageListData('/role/list', {
        offset: 0,
        size: 100
      })
      const { list: roleList } = roleRes.data
      // console.log(departmentList)
      // console.log(roleList)
      // 请求菜单
      const menuRes = await getPageListData('/menu/list', {})
      const { list: menuList } = menuRes.data
      // 2、保存数据
      commit('changeEntireDepartment', departmentList)
      commit('changeEntireRole', roleList)
      commit('changeEntireMenu', menuList)
    }
  },
  modules: {
    login,
    system,
    dashboard
  }
})

export function setupStore() {
  store.dispatch('login/loadLoaclLogin')
  // store.dispatch('getInitialDataAction') //放到用户登陆的时候加载数据
}

export function useStore(): Store<IStoreType> {
  return useVuexStore()
}

export default store
