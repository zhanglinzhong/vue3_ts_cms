export interface IDashboardState {
  topPanelData: any[]
  categoryGoodsCount: any[]
  categoryGoodsSales: any[]
  categoryGoodsFavor: any[]
  addressGoodsSales: any[]
}
