import { Module } from 'vuex'
import { IDashboardState } from './types'
import { IRootState } from '../../types'

import {
  getAmountList,
  getCategoryGoodsCount,
  getCategoryGoodsSale,
  getCategoryGoodsFavor,
  getAddressGoodsSale
} from '@/service/main/analysis/dashboard'

const dashboardModule: Module<IDashboardState, IRootState> = {
  namespaced: true,
  state() {
    return {
      topPanelData: [],
      categoryGoodsCount: [],
      categoryGoodsSales: [],
      categoryGoodsFavor: [],
      addressGoodsSales: []
    }
  },
  mutations: {
    changeTopPanelDatas(state, list) {
      state.topPanelData = list
    },
    changeCategoryGoodsCount(state, list) {
      state.categoryGoodsCount = list
    },
    changeCategoryGoodsSale(state, list) {
      state.categoryGoodsSales = list
    },
    changeCategoryGoodsFavor(state, list) {
      state.categoryGoodsFavor = list
    },
    changeAddressGoodsSale(state, list) {
      state.addressGoodsSales = list
    }
  },
  actions: {
    async getDashboardDataAction({ commit }) {
      const topPanelDataRes = await getAmountList()
      commit('changeTopPanelDatas', topPanelDataRes.data)

      const categoryGoodsCountRes = await getCategoryGoodsCount()
      commit('changeCategoryGoodsCount', categoryGoodsCountRes.data.list)
      const categoryGoodsSaleRes = await getCategoryGoodsSale()
      commit('changeCategoryGoodsSale', categoryGoodsSaleRes.data.list)
      const categoryGoodsFavorRes = await getCategoryGoodsFavor()
      commit('changeCategoryGoodsFavor', categoryGoodsFavorRes.data.list)
      const addressGoodsSaleRes = await getAddressGoodsSale()
      commit('changeAddressGoodsSale', addressGoodsSaleRes.data.list)
    }
  }
}

export default dashboardModule
