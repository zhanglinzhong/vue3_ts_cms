import LzDescriptions from './src/discriptions.vue'
import type { DescriptionProp } from './types/types'

export { DescriptionProp }
export default LzDescriptions
