import * as echarts from 'echarts'

// 注册地图数据
import chinaMapData from '../mapdata/china.json'
echarts.registerMap('china', chinaMapData)

export default function (el: HTMLElement) {
  const echartInstance = echarts.init(el, { renderer: 'svg' })

  const setOpthions = (options: echarts.EChartsOption) => {
    echartInstance.setOption(options)
  }

  window.addEventListener('resize', () => {
    echartInstance.resize()
  })

  const updateSize = () => {
    echartInstance.resize()
  }

  return {
    echartInstance,
    setOpthions,
    updateSize
  }
}
