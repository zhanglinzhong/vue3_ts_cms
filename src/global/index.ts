import { App } from 'vue'
import registerElement from './register-element'
import registerProperties from './register-properties'

export function globalRegister(app: App): void {
  // registerElement(app) //注册element
  app.use(registerElement) //注册element
  app.use(registerProperties) //注册element
}
