# 前后端分离项目

项目地址：http://coderlz.xyz

登陆账号：coderlz	密码：123456

服务器：1核 2G 1M

## 前端技术栈

| 开发工具      | Visual Studio Code                |
| ------------- | --------------------------------- |
| 编程语言      | TypeScript 4.x + JavaScript       |
| 构建工具      | Vite 2.x / Webpack5.x             |
| 前端框架      | Vue 3.x                           |
| 路由工具      | Vue Router 4.x                    |
| 状态管理      | Vuex 4.x                          |
| UI 框架       | Element Plus                      |
| 可视化        | Echart5.x                         |
| 富文本        | WangEditor                        |
| 工具库        | @vueuse/core + dayjs + countup.js |
| CSS 预编译    | Less / Sass                       |
| HTTP 工具     | Axios                             |
| Git Hook 工具 | husky                             |
| 代码规范      | EditorConfig + Prettier + ESLint  |
| 提交规范      | Commitizen + Commitlint           |
| 自动部署      | Centos + Jenkins + Nginx          |

## 后端技术栈

后端使用go语言fiber框架

后端所用到的库：

```go
github.com/gofiber/fiber/v2 v2.18.0
github.com/gofiber/jwt/v2 v2.2.7
github.com/golang-jwt/jwt/v4 v4.0.0
github.com/mojocn/base64Captcha v1.3.5
gorm.io/driver/mysql v1.1.2
gorm.io/gorm v1.21.14
```